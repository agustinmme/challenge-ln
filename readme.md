
# Challenge LN frontend jr

1. Armar el artículo (card) que sea lo suficientemente dinámico para contemplar al menos 3 opciones de renders.
2. Armar al menos un módulo contenedor de artículos completo, a elección. 



## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/agustinmme/challenge-ln
```

Go to the project directory

```bash
  cd challenge-ln
```

Execute watcher

```bash
  sass --watch SASS/index.scss:css/index.css
```

Run server with [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

Go to

```bash
  http://127.0.0.1:5500/view/1.html and http://127.0.0.1:5500/view/2.html
```


## Authors

- [Mansilla Agustin](https://www.linkedin.com/in/agustinmme/)

